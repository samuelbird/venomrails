
var geocoder = new google.maps.Geocoder();

function getLoc(){
	var wr = document.getElementById("errorRender")
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showLoc);
        
    } else { 
        wr.innerHTML = "Try a new browser.";
    }
}

function showLoc(position){
	getCity(position.coords.latitude, position.coords.longitude)
}

function getCity(lat,lng){
	var latlng = new google.maps.LatLng(lat, lng)
	var wr = document.getElementById("errorRender")
	geocoder.geocode({'location': latlng}, function(results, status){
		var countryLoc = results[0].address_components[5].short_name
		wr.innerHTML="location: " + countryLoc
	})
}