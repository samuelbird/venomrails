class CreateSnakes < ActiveRecord::Migration
  def change
    create_table :snakes do |t|
      t.string :name
      t.integer :venom
      t.string :location

      t.timestamps null: false
    end
  end
end
